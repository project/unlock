<?php

namespace Drupal\unlock\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {
  public const SETTINGS = 'unlock.settings';
  public const NETWORKS = [
      42161 => 'Arbitrum',
      56 => 'Binance',
      // 1337 => 'Ganache',
      42 => 'Kovan',
      // 31337 => 'Localhost',
      1 => 'MainNet (Ethereum)',
      // 10 => 'Optimism',
      // 69 => 'Optimistic Kovan',
      137 => 'Polygon',
      4 => 'RinkeBy',
      3 => 'Ropsten',
      100 => 'xDai',
    ];
  public const RPC_ENDPOINTS = [
      42161 => 'https://arb1.arbitrum.io/rpc', //'Arbitrum',
      56 => 'https://bsc-dataseed.binance.org/', //'Binance',
      // 1337 => 'Ganache',
      42 => 'https://kovan.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161', //'Kovan',
      // 31337 => 'Localhost',
      1 => 'https://mainnet.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161', //'MainNet (Ethereum)',
      // 10 => 'Optimism',
      // 69 => 'Optimistic Kovan',
      137 => 'https://rpc-mainnet.maticvigil.com/', //'Polygon',
      4 => 'https://rinkeby.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161', //'RinkeBy',
      3 => 'https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161', //'Ropsten',
      100 => 'https://rpc.xdaichain.com/', //'xDai',
    ];
  public const LOGIN_TEXT = 'Connect your wallet for @type block';
  public const CHECKOUT_TEXT = 'Purchase for @type this';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unlock_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);


    $form['unlock_lock_network'] = [
      '#type' => 'select',
      '#title' => $this->t('Network'),
      '#options' => self::NETWORKS,
      '#default_value' => $config->get('network') ?: 1,
      '#required' => TRUE,
    ];

    $urls = Drupal::state()->get('unlock_config_urls');
    if ($urls) {
      $urls = array_map(static function ($element) {
        return [
          '#type' => 'link',
          '#title' => $element,
          '#url' => Url::fromUri('internal:' . $element),
        ];
      }, $urls);
      $form['links'] = [
        '#theme' => 'item_list',
        '#items' => $urls,
        '#title' => t('Enabled unlock feature'),
      ];
    }


    // $form['unlock_lock_address'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Lock address'),
    //   '#description' => $this->t('This will be a blockchain address starting with &quot;0x…&quot;'),
    //   '#maxlength' => 64,
    //   '#size' => 64,
    //   '#default_value' => $config->get('address'),
    //   '#required' => TRUE,
    // ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      // ->set('address', $form_state->getValue('unlock_lock_address'))
      ->set('network', $form_state->getValue('unlock_lock_network'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}

# Unlock Protocol Drupal 8 Module

Unlock Protocol provides the ability to show/hide content based on whether the content has been "unlocked" by a payment with digital currency. This payment uses a browser-based extension wallet, and hence a "subscription" can be determined.

Visit the [Unlock Website](https://unlock-protocol.com/) for more details.

## Features

Support lock for:
  - Block (/admin/structure/block)
  - Content type (/admin/structure/types)
  - Field
  - Any configuration entity and any other entity which implements ThirdPartySettingsInterface
  - Adver Free experience - when block visible while unpaid

Global lock which can be overriden.
Text or template of links can be configured.
List of paths where enabled locks.


### Instructions

Use the Unlock Protocol's website to create a lock. Look for the dashboard link. Once created, copy the address to the lock, it will start with 0x… and look like a random hash.

Have an admin enter the lock address in the Unlock settings page or for block/content type/field.
